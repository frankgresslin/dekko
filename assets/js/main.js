/*jslint browser: true*/
/*global jQuery, document, window, location*/
(function ($) {
    "use strict";
   
	jQuery(document).ready(function ($) {
        
		// SMOOTH SCROLL		
			
		// on html5 test page
		$('a[href*="#"]:not([href="#"])').click(function () {
			
			var winWidth = $(window).width();
			
			// prevent interference with visual coposer tabs, tours & accordions
			if ($(this).data("vc-container") === undefined) {
				
				if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
				
					var target = $(this.hash);
				
					target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				
					if (target.length) {
						
						if ( 480 > winWidth ) {
							
							$('html, body, #outer, #inner, #push').animate({
							
									scrollTop: target.offset().top
							
							}, 1000);
							
						} else {
							
							$('html, body, #outer, #inner, #push').animate({
							
									scrollTop: target.offset().top - 60
							
							}, 1000);
							
						}
						
						return false;
					}
				}
			}
		});	
        
		
		// TYPE-IT
		// @link https://typeitjs.com/
		
		if ($('.type-it').length) {
			
			jQuery('.type-it').typeIt({
			   strings: ["We’ve been thinking"], 
			   loop: false
			});
		}
				
		
		// CONTACT FORM 7
		
		// error messages for footer form - placed over input in order to prevent 
		// the slide out footer to change it's height and dissapear behind #page 
		$('#content .wpcf7 .wpcf7-form-control-wrap').on('hover', function () {			
			$(this).find('.wpcf7-not-valid-tip').fadeOut(500);
			$(this).find('*').removeClass('wpcf7-not-valid');
		});
			
	});
	
}(jQuery));