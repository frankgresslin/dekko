<?php

/**
 *  THEME OPTIONS - SITE SEARCH
 *
 *  Contains:
 *  01 - fg_search_term_highlighting()
 *  02 - fg_search_page_header()
 *  03 - fg_search_again()
 *    
 *  @package include
 *  @since 	 1.0
 *  @link    https://codex.wordpress.org/Creating_a_Search_Page
 *  @version 1.0.0
 *   
 *  @TODO  	make fg_search_again() proof for any possible post type
 *			check for posttypes, exclude unwanted, check 'exclude_from_search'
 *			loop through post types and dynamically add checkbox
 */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


if ( ! function_exists( 'fg_search_term_highlighting' ) ) :
	
	/**
	 * 	SEARCH TERM HIGHLIGHTING 
	 *
	 *  Wrap the search term into a span tag if Relevanssi is not active
	 *
	 *  @usedby	 theme-options-posts.php
	 * 	@return  string  $excerpt  the modified excerpt
	 *	@link    https://wordpress.stackexchange.com/questions/16070/how-to-highlight-search-terms-without-plugin
	 */
	
	function fg_search_term_highlighting() {
		
		// if no excerpt exists, use the content, expand all shortcodes and strip all html
		$excerpt = ( ! empty( get_the_excerpt() ) ) ? get_the_excerpt() : strip_tags( do_shortcode( get_the_content() ) );
		$keys = implode( '|', explode( ' ', get_search_query() ) );
		$excerpt = preg_replace( '/(' . $keys .')/iu', '<span class="search-highlight">\0</span>', $excerpt );
	
		return $excerpt;	
	}

endif;



if ( ! function_exists( 'fg_search_page_header' ) ) :
	
	/**
	 * 	SEARCH PAGE SUB HEADER
	 *
	 *  showing results $from to $to of $total 
	 *
	 *  @return  string  $html
	 *
	 *  @usedby	search.php
	 *
	 *  @TODO  show only if there are more then 1 pages
	 */
	
	function fg_search_page_header() {
		
		if ( is_search() ) {
		
			// current page
			$current = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
			
			// posts per page
			$option_posts_per_page = get_option( 'posts_per_page' );
			
			// total pages
			global $wp_query;
			$total = $wp_query->found_posts;
			
			// number of pages
			$pages = (int) ceil( $total / $option_posts_per_page );
			
			// to
			if ( $pages == $current ) {
				$to = $total;		
			} else {
				$to = $current * $option_posts_per_page;
			}
			
			// from
			$from = ( $current * $option_posts_per_page ) - ( $option_posts_per_page - 1 );
			
		
			$html = '<header id="include-search-title">';
			$html .= 	'<h3>';
			$html .= 		'<span>' . __( "showing results", "include" ) . ' </span>';
			$html .= 		'<strong>' . $from . ' </strong>';
			$html .= 		'<span>' . __( "to", "include" ) . ' </span>';
			$html .= 		'<strong>' . $to . ' </strong>';
			$html .= 		'<span>' . __( "of", "include" ) . ' </span>';
			$html .= 		'<strong>' . $total . '</strong>';
			$html .= 	'</h3>';
			$html .= '</header>';
		
			echo $html;	
			
		}
	
	}
	
	add_action( 'include_before_loop', 'fg_search_page_header' );

endif;



if ( ! function_exists( 'fg_search_again' ) ) :

	/**
	 * 	SEARCH AGAIN BOX
	 *
	 *  search form below search results with seach filter that allows 
	 *  searches to be filtered by post type  
	 *
	 *  @return  string  $html
	 *
	 *  @usedby	search.php
	 */
	
	function fg_search_again() {
		
		if ( is_search() ) {
			
			$html = '<section id="include-search-again">';
			$html .= 	__( "If you have not found what you are looking for, try to refine your search: ", "include" );
			$html .= 	'<form id="searchform" class="search-form" role="search" method="get" action="' . esc_url( home_url() ) . '">';
			$html .= 		'<label>';
			$html .= 			'<span class="screen-reader-text">' . _x( "Search for:", "label", "include" ) . '</span>';
			$html .= 			'<input class="search-field" placeholder="' . _x( "Search &hellip;", "placeholder", "include" ) . '" value="' . get_search_query() . '" name="s" type="search">';
			
			if ( isset( $_GET['fg_post_type'] ) ) {
				
				$post = ( in_array( 'post', $_GET['fg_post_type'] ) ) ? 'checked="checked"' : '';
				$page = ( in_array( 'page', $_GET['fg_post_type'] ) ) ? 'checked="checked"' : '';
				
				$html .= '<div id="search-filter"> ';
				$html .= 	'<span>' . __( 'Filter: ', 'include' ) . '</span>';
				$html .= 	'<label><input type="checkbox" name="fg_post_type[]" value="post" ' . $post . '/>' . __( 'Posts', 'include' ) . '</label>';
				$html .=	 '<label><input type="checkbox" name="fg_post_type[]" value="page" ' . $page . '/>' . __( 'Pages', 'include' ) . '</label>';
				
				if ( post_type_exists( 'project' ) ) {
					
					$project = ( in_array( 'project', $_GET['fg_post_type'] ) ) ? 'checked="checked"' : '';
					$html .= 	'<label><input type="checkbox" name="fg_post_type[]" value="project" ' . $project . '/>' . __( 'Projects', 'include' ) . '</label>';
				}
				
				if ( post_type_exists( 'dt_portfolio' ) ) {
					
					$dt_portfolio = ( in_array( 'dt_portfolio', $_GET['fg_post_type'] ) ) ? 'checked="checked"' : '';
					$html .= 	'<label><input type="checkbox" name="fg_post_type[]" value="dt_portfolio" ' . $dt_portfolio . '/>' . __( 'Projects', 'include' ) . '</label>';
				}
				
				$html .= '</div> <!--search-filter-->';
			}
			
			$html .= 			'<input class="search-submit" value="Search" type="submit">';
			$html .= 		'</label>';
			$html .= 	'</form>';
			$html .= '</section> <!--search-again-->';
			$html .= '';
			$html .= '';
			
			echo $html;
			
		}
	}
	
	add_action( 'include_after_loop', 'fg_search_again', 2 );

endif;
