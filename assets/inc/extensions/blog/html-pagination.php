<?php

/**
 *  HTML IN HOME & ARCHIVES
 *    
 *  Contains:
 *  01 - fg_pagination()
 *  02 - fg_ajax_load_more()
 *    
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


if ( ! function_exists( 'fg_pagination' ) ) :
	
	/**
	 * 	PAGINATION
	 *
	 *  Echos the pagination 
	 *
	 *  @usedby	 archive-project.php
	 *  @usedby	 archive.php
	 *  @usedby	 author.php
	 *  @usedby	 home.php
	 *  @usedby	 search.php
	 *  @usedby	 taxonomy.php
	 */
	
	
	function fg_pagination() {
				
		if ( ! is_singular() ) {
			
			if ( function_exists( 'AjaxLoadMore' ) ) {
				
				fg_ajax_load_more();
				
			}  elseif ( function_exists( 'dt_paginator' ) ) {
						
				dt_paginator();
						
			} else {
				
				$html = '<div id="include-pagination">';
				$html .= 	get_the_posts_pagination( array(
								'mid_size' 			 => 2,
								// 'prev_text'          => __( '&#10096;', 'include' ),
								// 'next_text'          => __( '&#10097;', 'include' ),
								'prev_text'          => __( '', 'include' ),
								'next_text'          => __( '', 'include' ),
								));
				$html .= '</div>';
				
				echo $html;
				
			}		
		}
	}
	
	add_action( 'include_after_loop', 'fg_pagination', 1 );

endif;



if ( ! function_exists( 'fg_ajax_load_more' ) ) :

	/**
	 * 	AJAX LOAD MORE
	 *
	 *  Echos the ajax load more pagination 
	 *
	 *  @usedby	 fg_pagination()
	 */
	 
	function fg_ajax_load_more() {
		
			switch ( true ) {
		    case is_home():
		    	echo do_shortcode( '[ajax_load_more button_label="More&hellip;" post_type="post" scroll_distance="4000" offset="' . get_option( 'posts_per_page' ) . '" pause="true" post_format="standard"]' );
		        break;
		    case is_post_type_archive( 'project' );
		    	echo do_shortcode( '[ajax_load_more button_label="More&hellip;" scroll_distance="4000" post_type="' . get_post_type() . '" offset="' . get_option( 'posts_per_page' ) . '" pause="true"]' );
		        break;
		    case is_archive():
		    	echo do_shortcode( '[ajax_load_more button_label="More&hellip;" post_type="post" scroll_distance="4000" offset="' . get_option( 'posts_per_page' ) . '" pause="true" post_format="standard"]' );
		        break;
		    case is_author():
		    	echo do_shortcode('[ajax_load_more id="author_archive" button_label="Older Posts" scroll_distance="4000" post_type="' . get_post_type() . '" offset="' . get_option( 'posts_per_page' ) . '" author="' . get_the_author_meta('ID')  . '" pause="true"]');
		        break;
		    case is_search():
		    	echo do_shortcode('[ajax_load_more id="relevanssi" button_label="More&hellip;" scroll_distance="4000" offset="' . get_option( 'posts_per_page' ) . '" post_type="post, page, project" pause="true" search="' . $_GET["s"] . '"]');
		        break;
		    default: 
		    	return false;
		    	
		}	
	}

endif;
