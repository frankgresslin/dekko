<?php

/**
 *  POSTS THUMBNAIL
 *    
 *  Contains:
 *  01 - fg_post_thumbnail()
 *    
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


if ( ! function_exists( 'fg_post_thumbnail' ) ) :
	
	/**
	 * 	POST THUMBNAIL
	 *
	 *  Used post thumbnail if present, otherwise site logo. 
	 * 	Opens large post thumbnail in lightbox on singular
	 *
	 *  @usedby	 content-page.php
	 *  @usedby	 content-post-narrow.php
	 *  @usedby	 content-post.php
	 *  @usedby	 content-project.php
	 *  @usedby	 content-search.php
	 *  @usedby	 content-single-project.php
	 *  @usedby	 content-single.php
	 *  @return  $html  string  anchor with background image
	 */
	
	function fg_post_thumbnail() {
		
		// Magnific Popup attribute
		$lightbox = ( is_single() ) ? 'rel="Magnific"' : '';
		
		// image link to full imagde for popup on single, permalink on home 
		$link_url = ( is_single() ) ? get_the_post_thumbnail_url( get_the_ID(), 'full' ) : get_permalink();
		
		// full size for single, custom size for home
		$image_size = ( is_single() ) ? 'full' : 'fg-blog-thumbnail';
		
		// prevent anchor link from executing on single
		$on_click = ( is_single() ) ? 'event.preventDefault();' : '';
		
		// caption
		$caption = ( get_the_post_thumbnail_caption() && is_single() ) ? '<figcaption>' . get_the_post_thumbnail_caption() . '</figcaption>' : '';
		
		// use post title as alt tag if no alt is provided
		$thumb_id = get_post_thumbnail_id( get_the_ID() );
		$thumb_alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
		$alt = ( ! empty( $thumb_alt ) ) ? $thumb_alt : get_the_title();
		
		$html = '';
			
		// enables fluid scaling without css height
		$placeholder = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAQAAADTdEb+AAACHElEQVR42u3SMQ0AAAzDsJU/6aGo+tgQouSgIBJgLIyFscBYGAtjgbEwFsYCY2EsjAXGwlgYC4yFsTAWGAtjYSwwFsbCWGAsjIWxwFgYC2OBsTAWxgJjYSyMBcbCWBgLjIWxMBYYC2NhLDAWxsJYYCyMhbHAWBgLY4GxMBbGAmNhLIwFxsJYGAuMhbEwFhgLY2EsMBbGwlhgLIyFscBYGAtjgbEwFsYCY2EsjAXGwlgYC2OBsTAWxgJjYSyMBcbCWBgLjIWxMBYYC2NhLDAWxsJYYCyMhbHAWBgLY4GxMBbGAmNhLIwFxsJYGAuMhbEwFhgLY2EsMBbGwlhgLIyFscBYGAtjgbEwFsYCY2EsjAXGwlgYC4yFsTAWGAtjYSwwFsbCWGAsjIWxwFgYC2OBsTAWxgJjYSyMBcbCWBgLjIWxMBbGAmNhLIwFxsJYGAuMhbEwFhgLY2EsMBbGwlhgLIyFscBYGAtjgbEwFsYCY2EsjAXGwlgYC4yFsTAWGAtjYSwwFsbCWGAsjIWxwFgYC2OBsTAWxgJjYSyMBcbCWBgLjIWxMBYYC2NhLDAWxsJYYCyMhbHAWBgLY4GxMBbGAmNhLIwFxsJYGAuMhbEwFhgLY2EsjCUBxsJYGAuMhbEwFhgLY2EsMBbGwlhgLIyFscBYGAtjgbEwFsYCY2EsjAXGwlgYC4yFsTAWGAtjYSwwFsbCWGAsjIWxwFjsPeVaAS0/Qs6MAAAAAElFTkSuQmCC';
		
		if ( has_post_thumbnail( get_the_ID() ) ) {
			
			$html .= '<figure class="include-post-thumbnail">';
			$html .= 	'<a href="' . $link_url . '" style="background: url(' . get_the_post_thumbnail_url( get_the_ID(), $image_size ) . ');"' . $lightbox . '>';
			$html .= 		'<img src="' . $placeholder . '" alt="' . esc_attr( $alt ) . '" />';	
			$html .= 	'</a>';
			// $html .= 	$caption;
			$html .= '</figure>';
						
		} else {
			
			$html .= '<figure class="include-post-thumbnail">';
			$html .= 	'<a href="' . get_permalink() . '" style="background: url(' . fg_get_option( 'site_logo' ) . ');" class="no-thumbnail" onclick="' . $on_click . '">';
			$html .= 		'<img src="' . $placeholder . '" alt="' . esc_attr( $alt ) . '" />';	
			$html .= 	'</a>';
			$html .= '</figure>';
			$html = '';
		}
		
		echo $html;
		
	}

endif;
