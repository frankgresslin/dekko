<?php

/**
 *  THEME OPTIONS - BLOG
 *
 *  Contains:
 *  01 - fg_custom_excerpt()
 *  02 - fg_new_excerpt_more()
 *  03 - fg_custom_excerpt_length()
 *  04 - fg_allowedtags()
 *  05 - fg_html_in_excerpt()
 *    
 *  @package include
 *  @since 	 1.0
 *  @version 0.1
 *  @TODO 	 review
 */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


if ( ! function_exists( 'fg_custom_excerpt' ) ) :

	/**
	 * 	CUSTOM EXCERT
	 *
	 *  Sets excerpt lenght - from content or excerpt - and adds read more link
	 *
	 *  @usedby	 content-page.php
	 *  @usedby	 content-post.php
	 *  @usedby	 content-project.php
	 *  @param   ineger  $limit
	 *  @param	 bool  $echo 
	 *  @return  string  anchor html
	 */
	
	function fg_custom_excerpt( $limit, $echo = true ) {
	
		// if no excerpt exists, use the content, expand all shortcodes and strip all html
		$excerpt = ( ! empty( get_the_excerpt() ) ) ? get_the_excerpt() : strip_tags( do_shortcode( get_the_content() ) );
		
		// add search term highlighting on search page if Relevaansi is not installed
		$excerpt = ( ( is_search() ) && ( ! function_exists( '_relevanssi_install' ) )  ) ? fg_search_term_highlighting() : $excerpt;
		$excerpt =  explode( ' ', $excerpt, $limit );
		
		// remove last item in array if the excerpt has more words then set in the the limit
		if ( count( $excerpt ) >= $limit ) {
			
			array_pop( $excerpt );
			
			$excerpt = implode( " ", $excerpt ) . '<span class="hellip">...</span> <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __( 'read more &rarr;', 'include' ) . '</a>';
			
		} else {
			
			$excerpt = implode( " ", $excerpt ) . '<span class="hellip">...</span> <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __( 'read more &rarr;', 'include' ) . '</a>';
			
		}	
		
		$excerpt = preg_replace( '`[[^]]*]`', '', $excerpt );
		
		if ( $echo )  {
			
			echo '<div class="excerpt">' . $excerpt . '</div>';
			
		} else {
			
			return '<div class="excerpt">' . $excerpt . '</div>';
			
		}	
	}

endif;



if ( ! function_exists( 'fg_new_excerpt_more' ) ) :

	/**
	 * 	EXCERPT MORE
	 *
	 *  link added after the_excerpt() if no excerpt exists and content is used with the <!--more--> tag
	 *
	 *  @param	 $more  string 
	 *  @return  string  anchor with permalink
	 */
	
	function fg_new_excerpt_more( $more ) {
		
		return '<span class="hellip">...</span> <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __( 'read more &rarr;', 'include' ) . '</a>';
		
	}
	
	add_filter( 'excerpt_more', 'fg_new_excerpt_more' );

endif;



if ( ! function_exists( 'fg_custom_excerpt_length' ) ) :
	
	/**
	 * 	EXCERPT LENGTH
	 *
	 *  the_excerpt() lenth of the content if no excerpt exists - ignores the <!--more--> tag
	 *
	 *  @param	 $length  ineger 
	 *  @return  ineger  new length
	 */
	
	function fg_custom_excerpt_length( $length ) {
		return 43;
	}
	
	add_filter( 'excerpt_length', 'fg_custom_excerpt_length', 999 );

endif;



if ( ! function_exists( 'fg_html_in_excerpt' ) ) :

	/**
	 * 	ENABLE HTML IN EXCERPTS
	 *
	 *  for post formats
	 *
	 *  @param	 $fg_excerpt  string  the excerpt  
	 *  @return  $raw_excerpt string  anchor html
	 */
	
	function fg_allowedtags() {
	    return '<span>,<p>,<br>,<a>,<img>,<video>,<audio>,<figure>,<figcaption>,<source>,<blockquote>,<div>,<iframe>,<script>,<time>'; 
	}
	
	function fg_html_in_excerpt( $fg_excerpt ) {
		    	   
		$raw_excerpt = $fg_excerpt;
		
		if ( has_post_format( array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) ) ) {
		
		    if ( '' == $fg_excerpt ) {
		
		        $fg_excerpt = get_the_content('');
		        $fg_excerpt = strip_shortcodes( $fg_excerpt );
		        $fg_excerpt = apply_filters('the_content', $fg_excerpt);
		        $fg_excerpt = str_replace(']]>', ']]&gt;', $fg_excerpt);
		        $fg_excerpt = strip_tags($fg_excerpt, fg_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */
		
		        //Set the excerpt word count and only break after sentence is complete.
		            $excerpt_word_count = 75;
		            $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
		            $tokens = array();
		            $excerptOutput = '';
		            $count = 0;
		
		            // Divide the string into tokens; HTML tags, or words, followed by any whitespace
		            preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $fg_excerpt, $tokens);
		
		            foreach ($tokens[0] as $token) { 
		
		                if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) { 
		                // Limit reached, continue until , ; ? . or ! occur at the end
		                    $excerptOutput .= trim($token);
		                    break;
		                }
		
		                // Add words to complete sentence
		                $count++;
		
		                // Append what's left of the token
		                $excerptOutput .= $token;
		            }
		
		        $fg_excerpt = trim(force_balance_tags($excerptOutput));
		
		            $excerpt_end = ' <a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf(__( 'Read more about: %s &nbsp;&raquo;', 'include' ), get_the_title()) . '</a>'; 
		            $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end); 
		
		            //$pos = strrpos($fg_excerpt, '</');
		            //if ($pos !== false)
		            // Inside last HTML tag
		            //$fg_excerpt = substr_replace($fg_excerpt, $excerpt_end, $pos, 0); /* Add read more next to last word */
		            //else
		            // After the content
		            $fg_excerpt .= $excerpt_more; /*Add read more in new paragraph */
		
		        return do_shortcode( $fg_excerpt );   
		    }
		    	    
		    return do_shortcode( apply_filters('fg_custom_wp_trim_excerpt', $fg_excerpt, $raw_excerpt) );	
		
		} else {
			
			return do_shortcode( $raw_excerpt );		
			
		} 
	} 
	
	// remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
	// add_filter( 'get_the_excerpt', 'fg_html_in_excerpt' ); 

endif;
