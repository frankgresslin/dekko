<?php
	
/**
 *  THEME OPTIONS - POST META LINKS FOR ANY POST TYPE
 *
 *  Contains:
 *  01 - fg_post_meta
 *  02 - fg_featured_post
 *  03 - fg_date_archive_link
 *  04 - fg_author_archive_link
 *  05 - fg_category_archive_links
 *  06 - fg_tag_archive_links
 *  07 - fg_taxonomy_archive_links
 *  08 - fg_custom_field_archive_link
 *  09 - fg_comments_link
 *    
 *  @package include
 *  @since 	 1.0
 *  @link    https://codex.wordpress.org/Post_Meta_Data_Section
 *  @version 1.0.0
 */
 
 // File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


if ( ! function_exists( 'fg_post_meta' ) ) :

    /**
     * 	The Post Meta Links
     */

	function fg_post_meta() {
				
		$html = '<nav class="include-post-meta">';
			
			if ( 'project' == get_post_type()  ) {
				
				$html .= fg_featured_post();
				$html .= fg_date_archive_link();
				$html .= fg_author_archive_link();
			 	$html .= fg_taxonomy_archive_links( 'project_category', 'categories', 99 );
			 	$html .= fg_taxonomy_archive_links( 'project_tag', 'tags', 1 );
			 	$html .= fg_custom_field_archive_link( 'text_input' );
			 	$html .= fg_comments_link();
				
			} elseif ( 'dt_portfolio' == get_post_type()  ) {
				
				$html .= fg_featured_post();
				$html .= fg_date_archive_link();
				$html .= fg_author_archive_link();
			 	$html .= fg_taxonomy_archive_links( 'dt_portfolio_category', 'categories', 99 );
			 	$html .= fg_comments_link();
				
			} else {
				
				$html .= fg_featured_post();
				$html .= fg_date_archive_link();
				$html .= fg_author_archive_link();
			 	$html .= fg_category_archive_links();
			 	$html .= fg_tag_archive_links();
			 	$html .= fg_comments_link();
			}
		
		$html .= '</nav>';
		
		echo $html;	
			
	}
	
endif;



if ( ! function_exists( 'fg_featured_post' ) ) :

    /**
     * 	Sticky Posts
     *  
     * 	@return $html  string  - span tag
     */

	function fg_featured_post() {		
		
		$featured = get_post_meta( get_the_ID(), 'featured_' . get_post_type() , true );		
		$featured = ( 'yes' == $featured ) ? true : false;
		
		if ( is_sticky() || $featured ) {
			
			$html = '<span class="featured-post">';
			$html .= 		__( 'Featured', 'include' );
			$html .= '</span>';
			
			return $html;
			
		}
	}
	
endif;



if ( ! function_exists( 'fg_date_archive_link' ) ) :

    /**
     * 	Date Archive Link 
     *
     * 	@return  $html  string  date archive link
     *	@URL  https://codex.wordpress.org/Formatting_Date_and_Time
     */

	function fg_date_archive_link() {
		
		$pto = get_post_type_object( get_post_type() );
		$archive = ( 'post' == get_post_type() ) ? '' : '/' . $pto->rewrite['slug'];
		
		$html = '<span class="meta-date">';
		$html .= 	'<a href="' . esc_url( get_home_url() . $archive . '/' . get_the_date('Y') . '/' . get_the_date('m') . '/' ) . '">';
		$html .= 		'<time datetime="' . get_the_date( 'c' ) . '">';
		$html .= 			get_the_date( 'd/M/Y' );
		$html .= 		'</time>';
		$html .= 	'</a>';
		$html .= '</span>';
		
		return $html;
		
	}

endif;



if ( ! function_exists( 'fg_author_archive_link' ) ) :

    /**
     * 	Author Archive Link 
     *
     *	Setting query string to filter post types on author archives
     *
     * 	@return  $html  string  author archive link
     */
     
	function fg_author_archive_link() {
		
		$posttype = ( 'post' == get_post_type() ) ? '' : '/?posttype=' . get_post_type();
					
		$html = '<span class="meta-author">';
		$html .= 	'by <a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) . $posttype ) . '">' . get_the_author() . '</a>';
		$html .= '</span>';
		
		return $html;
		
	}

endif;



if ( ! function_exists( 'fg_category_archive_links' ) ) :

    /**
     * 	Post Categories
     *  
     *  Remove 'uncategorised' category
     *  
     * 	@return $html  string  - category links
     */

	function fg_category_archive_links() {
		
		$categories = ( get_the_category() ) ? get_the_category() : NULL;
		$links = '';
		$html = '';
		$i = 0;
		$x = 0;
		
		if ( isset( $categories ) ) {
			
			// how many categories except 'uncategorized' are there? 
			foreach ( $categories as $category ) {
				if ( $category->slug != 'uncategorised' ) { $x++; }
			}
			
			foreach ( $categories as $category ) {
				
				// loop category array and build liks of all categories except for uncategorised 
				if ( $category->slug != 'uncategorised' ) {
					
					$links .= 	'<a href="' . esc_url( get_home_url() .  '/category/' . $category->slug  ) . '">' . $category->name . '</a>';
					
					$i++;
					
					// break if all links are built
					if ( $i++ >= $x ) {
						break;
					}
					
					// add a comma after each link exept after the last ctegory
					$links .= ', ';
				}			
			}
			
			// return link list with a wrapper
			if ( $i >= 1 ) {
				
				$html = '<span class="meta-categories">';
					$html .= $links;
				$html .= '</span>';
				
			}
			
			return $html;	
			
		}
	}
	
endif;



if ( ! function_exists( 'fg_tag_archive_links' ) ) :

    /**
     * 	Post Tags
     *  
     * 	@return  string  $html  - tag links
     */

	function fg_tag_archive_links() {
		
		$tags = ( get_the_tags() ) ? get_the_tags() : NULL;
		
		if ( isset( $tags ) ) {
			
			$html = '<span class="meta-tags">';
			
				$html .= get_the_tag_list( '', ', ' );
			
			$html .= '</span>';
			
			return $html;
			
		}		
	}
	
endif;



if ( ! function_exists( 'fg_taxonomy_archive_links' ) ) :

    /**
     * 	Custom Taxonomies Term Links
     *  
     *	@param  string  $taxonomy  - the taxonomy	
     *	@param  string  $tax_type  - tag or category - used for styling
     *	@param  ineger  $i  - limit e.g. if many terms break layout
     * 	@return string  $html  - custom taxonomy term links
     */

	function fg_taxonomy_archive_links( $taxonomy, $tax_type, $i = 0 ) {
		
		$terms = ( get_the_terms( get_the_ID(), $taxonomy ) ) ? get_the_terms( get_the_ID(), $taxonomy ) : NULL;
		$x = 0;
				
		if ( isset( $terms ) ) {
			
			$html = '<span class="meta-' . $tax_type . '">';
		
			foreach ( $terms as $term ) {
				
				$x++;
				
				$html .= '<a href="' . esc_url( get_term_link( $term ) ) . '">' .  $term->name . '</a>';
				
				if ( ( $i == $x ) || ( $term->count == $x ) ) {
					break;
				}
				
				$html .= ', ';
			}
			
			$html .= '</span>';
			
			return $html;
			
		}		
	}
	
endif;



if ( ! function_exists( 'fg_custom_field_archive_link' ) ) :

    /**
     * 	Custom Meta Field Archive Link 
     *
     *	@param  $meta_field  string  custom meta-field id
     * 	@return  $html  string  custom meta-field link
     */

	function fg_custom_field_archive_link( $meta_field ) {
		
		$pto = get_post_type_object( get_post_type() );
		$meta_value = ( get_post_meta( get_the_ID(), $meta_field, true ) ) ? sanitize_text_field( get_post_meta( get_the_ID(), $meta_field, true ) ) : NULL;
		
		if ( isset( $meta_value ) ) {
			
			$html = '<span class="meta-value">';
			$html .= 	'<a href="' . esc_url( get_home_url() .  '/' . $pto->rewrite['slug'] . '/?metafield=' . $meta_field . '&metavalue=' . $meta_value ) . '">' . $meta_value . '</a>';
			$html .= '</span>';
		
			return $html;
			
		}
	}

endif;



if ( ! function_exists( 'fg_comments_link' ) ) :

    /**
     * 	Comments Popup Link 
     *
     * 	@return  $html  string  the comment popup link
     *  @link    https://codex.wordpress.org/Function_Reference/comments_popup_link
     */

	function fg_comments_link() {
		
		if ( comments_open() ) { 
			
			// don't echo out here
			ob_start();
			comments_popup_link( __( 'Leave a Comment', 'include' ), __( '1 Comment', 'include' ), __( '% Comments', 'include' ), '', __( 'Comments are off', 'include' ) ); 
			$comments_popup = ob_get_contents();
			ob_end_clean();
						
			$html = '<span class="meta-comments">';
				$html .= $comments_popup;
			$html .= '</span>';
			
			return $html;
			
		}
	}

endif;
