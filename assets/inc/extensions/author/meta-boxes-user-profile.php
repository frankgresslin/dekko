<?php

/**
 *  USER PROFILE META FIELDS
 *
 *  Contains:
 *  01 - fg_add_user_contactmethods()
 *  02 - fg_register_form_display_extra_fields()
 *  03 - fg_user_register_save_extra_fields()
 *    
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


$extra_fields =  array( 
	array( 'linkedin', __( 'LinkedIn URL', 'include' ), false ),
	array( 'phone', __( 'Phone Number', 'include' ), true ),
	array( 'twitter', __( 'Twitter Name', 'include' ), true ),
	array( 'facebook', __( 'Facebook URL', 'include' ), true )
);


/**
 * Add custom users custom contact methods
 *
 * @return      void
*/

function fg_add_user_contactmethods( $user_contactmethods ) {

	global $extra_fields;
	
	// Display each field
	foreach( $extra_fields as $field ) {
		
		if ( ! isset( $contactmethods[ $field[0] ] ) ) {
			
			$user_contactmethods[ $field[0] ] = $field[1];
			
		}
	}

    return $user_contactmethods;
}

add_filter( 'user_contactmethods', 'fg_add_user_contactmethods' );



/**
 * Show custom fields on registration page
 *
 * Show custom fields on registration if field third parameter is set to true
 *
 * @return      void
*/

function fg_register_form_display_extra_fields() {
    
    global $extra_fields;

    // Display each field if 3rd parameter set to "true"
    foreach( $extra_fields as $field ) {
	    
    	if ( true == $field[2] ) { 
	    	
    		if ( isset( $_POST[ $field[0] ] ) ) { 
	    		
	    		$field_value = $_POST[ $field[0] ]; 
	    		
	    	} else { 
		    	
		    	$field_value = ''; 
		    }
		    
    	?>
    	
	    <p>
            <label for="<?php echo $field[0]; ?>"><?php echo $field[1]; ?><br />
            	<input type="text" name="<?php echo $field[0]; ?>" id="<?php echo $field[0]; ?>" class="input" value="<?php echo $field_value; ?>" size="20" />
            </label>
	    </p>
	    
	    <?php
    	}
    }
}

add_action( 'register_form', 'fg_register_form_display_extra_fields' );



/**
 * Save field values
 *
 * @return      void
*/

function fg_user_register_save_extra_fields( $user_id, $password = '', $meta = array() )  {

    global $extra_fields;
    
    $userdata       = array();
    $userdata['ID'] = $user_id;
    
    // Save each field
    foreach( $extra_fields as $field ) {
	    
    	if ( true == $field[2] ) {
	    	 
	    	$userdata[ $field[0] ] = $_POST[ $field[0] ];
	    	
	    }
	}

    $new_user_id = wp_update_user( $userdata );
}

add_action( 'user_register', 'fg_user_register_save_extra_fields', 100 );
