<?php

/**
 *  CUSTOM POST TYPE - SECTIONS
 *
 *  Contains:
 *  01 - fg_sections_custom_post_type()
 *  02 - fg_sections_rewrite_flush()
 *  03 - fg_sections_updated_messages()
 *  04 - fg_sections_change_post_title()
 *    
 *  @package include
 *  @since 	 1.0
 *  @link    https://codex.wordpress.org/Post_Types#Custom_Post_Types
 *  @version 0.1
 *  @TODO 	 review
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


if ( ! function_exists( 'fg_sections_custom_post_type' ) ) :

	/**
	 * 	Register Custom Post Type 
	 *
	 * 	@return  $args  array  
	 *	@link    hhttps://codex.wordpress.org/Function_Reference/register_post_type
	 */

	function fg_sections_custom_post_type() {
	
	  $labels = array(
	    'name'               		=> _x( 'Sections', 'post type general name', 'include' ),
	    'singular_name'      		=> _x( 'Section', 'post type singular name', 'include' ),
	    'menu_name'          		=> _x( 'Sections', 'admin menu', 'include' ),
	    'name_admin_bar'     		=> _x( 'Section', 'add new on admin bar', 'include' ),
	    'add_new'            		=> _x( 'Add New', 'section', 'include' ),
	    'add_new_item'       		=> __( 'Add New Section', 'include' ),
	    'new_item'           		=> __( 'New Section', 'include' ),
	    'edit_item'          		=> __( 'Edit Section', 'include' ),
	    'view_item'          		=> __( 'View Section', 'include' ),
	    'all_items'          		=> __( 'All Sections', 'include' ),
	    'search_items'       		=> __( 'Search Sections', 'include' ),
	    'parent_item_colon'  		=> __( 'Parent Sections:', 'include' ),
	    'not_found'          		=> __( 'No Sections found.', 'include' ),
	    'not_found_in_trash' 		=> __( 'No Sections found in trash.', 'include' ),
	  );
	
	  $args = array(
	    'labels'					=> $labels,
	    'description'				=> __( 'List of sections', 'include' ),
	    'public'					=> true,
	    'exclude_from_search'		=> true,
	    'publicly_queryable'		=> false,
	    'show_ui'					=> true,
	    'show_in_menu'				=> true,
	    'show_in_nav_menus'			=> false,
	    'show_in_admin_bar'			=> false,
	    'query_var'					=> true,
	    'rewrite'					=> array( 'slug' => 'section' ),
	    'capability_type'			=> 'post',
	    'has_archive'				=> false,
	    'hierarchical'				=> false,
	    'menu_position'				=> 12,
	    'menu_icon'					=> 'dashicons-layout',
		// 'menu_icon'				=> get_stylesheet_directory_uri() . '/assets/admin/images/generic.png',
	    'supports'					=> array( 'title','editor' )
	  );
	  
	  $args = apply_filters( "include_section_args", $args );
	
	  register_post_type( 'section', $args );
	  
	}
	
	add_action( 'init', 'fg_sections_custom_post_type' );

endif;



if ( ! function_exists( 'fg_sections_rewrite_flush' ) ) :

	/**
	 * 	Flushes WordPress rewrite rules 
	 *	@link  https://codex.wordpress.org/Function_Reference/flush_rewrite_rules
	 */

	function fg_sections_rewrite_flush() {
	    flush_rewrite_rules();
	}
	
	add_action( 'after_switch_theme', 'fg_sections_rewrite_flush' );

endif;



if ( ! function_exists( 'fg_sections_updated_messages' ) ) :

	/**
	 * 	Update Messages 
	 *
	 *	@param   $messages  array  - default messages array
	 *	@return  $messages  array  - messages array with added sections messages
	 *	@link    https://codex.wordpress.org/Function_Reference/register_post_type
	 */

	function fg_sections_updated_messages( $messages ) {
		
		$post             = get_post();
		$post_type        = get_post_type( $post );
		$post_type_object = get_post_type_object( $post_type );
		
		$messages['section'] = array(
			0  => '', // Unused. Messages start at index 1.
			1  => __( 'Section updated.', 'include' ),
			2  => __( 'Custom field updated.', 'include' ),
			3  => __( 'Custom field deleted.', 'include' ),
			4  => __( 'Section updated.', 'include' ),
			/* translators: %s: date and time of the revision */
			5  => isset( $_GET['revision'] ) ? sprintf( __( 'Section restored to revision from %s', 'include' ), wp_post_revision_title( (int) sanitize_text_field( $_GET['revision'] ), false ) ) : false,
			6  => __( 'Section published.', 'include' ),
			7  => __( 'Section saved.', 'include' ),
			8  => __( 'Section submitted.', 'include' ),
			9  => sprintf(
				__( 'Section scheduled for: <strong>%1$s</strong>.', 'include' ),
				// translators: Publish box date format, see http://php.net/date
				date_i18n( __( 'M j, Y @ G:i', 'include' ), strtotime( $post->post_date ) )
			),
			10 => __( 'Section draft updated.', 'include' ),
		);
	
		if ( $post_type_object->publicly_queryable ) {
			$permalink = get_permalink( $post->ID );
	
			$view_link = sprintf( ' <a href="%s" target="_blank" rel="noopener noreferrer">%s</a>', esc_url( $permalink ), __( 'View Section', 'include' ) );
			$messages[ $post_type ][1] .= $view_link;
			$messages[ $post_type ][6] .= $view_link;
			$messages[ $post_type ][9] .= $view_link;
	
			$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
			$preview_link = sprintf( ' <a href="%s" target="_blank" rel="noopener noreferrer">%s</a>', esc_url( $preview_permalink ), __( 'Preview Section', 'include' ) );
			$messages[ $post_type ][8]  .= $preview_link;
			$messages[ $post_type ][10] .= $preview_link;
		}
	
		return $messages;
		
	}
	
	// add_filter( 'post_updated_messages', 'fg_sections_updated_messages' );

endif;



if ( ! function_exists( 'fg_sections_change_post_title' ) ) :

	/**
	 * 	Post Title Placeholder 
	 *
	 *	@param   $title  string  - the default placeholder text
	 *	@return  $title  string  - the updated placeholder text
	 *	@link    https://developer.wordpress.org/reference/hooks/enter_title_here/
	 */

	function fg_sections_change_post_title( $title ) {
	
	    $screen = get_current_screen();
	    
	    if ( $screen->post_type == 'section' ) {
	    
	        $title = apply_filters( "include_section_title", __( 'Enter Section Title Here', 'include' ) ); ;
	    }
	    
	    return $title;
	    
	}
	
	add_filter( 'enter_title_here', 'fg_sections_change_post_title' );

endif;
