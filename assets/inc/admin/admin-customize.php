<?php

/**
 *  ADMIN CUSTOMISATIONS
 *
 *  Contains:
 *  01 - fg_modify_columns()
 *  02 - fg_custom_column_content()
 *  03 - fg_custom_admin_bar_logo()
 *
 *  @package include
 *  @since   1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	ADMIN COLUMNS
 */

if ( ! function_exists( 'fg_modify_columns' ) ) :

	/**
	 * 	Add / Remove Admin Column 
	 */

	function fg_modify_columns( $columns ) {
		
		// @TODO - remove column
		unset( $columns[ 'cs_replacement' ] );
		
		$new_columns = array( 'fg_post_thumbnail' => __( 'Image', 'include' ) );
		
		$filtered_columns = array_merge( $columns, $new_columns );
		
		return $filtered_columns;
	  
	}
	
	add_filter( 'manage_posts_columns' , 'fg_modify_columns' );

endif;



if ( ! function_exists( 'fg_custom_column_content' ) ) :

	/**
	 * 	Output values 
	 */

	function fg_custom_column_content( $column, $post_id ) {
		
		if ( $column == 'fg_post_thumbnail' ) {
		
			echo the_post_thumbnail( array( 50, 50 ) );
		
		}
	}
	
	add_action( 'manage_posts_custom_column', 'fg_custom_column_content', 10, 2 );

endif;



/**
 * 	ADMIN BAR LOGO
 */

if ( ! function_exists( 'fg_custom_admin_bar_logo' ) ) :

	function fg_custom_admin_bar_logo() {
		
		echo '
			<style type="text/css">
			
				#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
					
					background-image: url(' . fg_get_option( 'site_logo' ) . ') !important;
					background-position: 0 0;
					background-size: contain;
					color: rgba(0, 0, 0, 0);
					
					  -webkit-filter: invert(100%);
					  	 -moz-filter: invert(100%);
					  	   -o-filter: invert(100%);
					  	  -ms-filter: invert(100%);
					  	  	  filter: invert(100%);
				}
				
			</style>
		';
	}
	 
	add_action( 'wp_before_admin_bar_render', 'fg_custom_admin_bar_logo' );

endif;
