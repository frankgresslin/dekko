<?php

 /**
  *  ADMIN RESET
  *
  *  Remove unnecessary items from the wordpress admin
  *
  *  Contains:
  *  01 - fg_remove_meta_boxes()
  *  02 - fg_remove_dashboard_meta()
  *  03 - fg_remove_from_admin_toolbar()
  *    
  *  @package include
  *  @since   1.0
  *  @version 1.0.1
  * 
  *  @link  http://www.wpexplorer.com/customize-wordpress-admin-dashboard/
  *  @link  https://digwp.com/2010/10/customize-wordpress-dashboard/
  */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	META BOXES
 */

function fg_remove_meta_boxes() {
	
	if ( ! current_user_can( 'manage_options' ) || is_admin() ) {
			
		// PAGES
		
		remove_meta_box( 'mymetabox_revslider_0', 'page', 'normal' );
		
		
		// POSTS
		
	// 	remove_meta_box( 'authordiv','post','normal' );
	// 	remove_meta_box( 'commentsdiv','post','normal' );
	// 	remove_meta_box( 'commentstatusdiv','post','normal' );
	// 	remove_meta_box( 'trackbacksdiv','post','normal' );
		remove_meta_box( 'postcustom','post','normal' );
		remove_meta_box( 'slugdiv','post','normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'post', 'normal' );
		
		
		// CUSTOM POST TYPE 'SECTION'
		
		// wp slug
		remove_meta_box( 'slugdiv', 'section', 'normal' );
		
		// custom sidebars
		remove_meta_box( 'customsidebars-mb', 'section', 'side' );
		
		// revolution slider
		remove_meta_box( 'mymetabox_revslider_0', 'section', 'normal' );
		
		// yoast seo
		remove_meta_box( 'wpseo_meta', 'section', 'normal' );
		
		// members
		remove_meta_box( 'members-cp', 'section', 'advanced' ); 

		
		// CUSTOM POST TYPE 'PROJECT'
		
		remove_meta_box( 'authordiv','dt_portfolio','normal' );
		remove_meta_box( 'commentsdiv','dt_portfolio','normal' );
	// 	remove_meta_box( 'commentstatusdiv','dt_portfolio','normal' );	
	// 	remove_meta_box( 'postcustom','dt_portfolio','normal' );
	// 	remove_meta_box( 'postexcerpt','dt_portfolio','normal' );
		remove_meta_box( 'revisionsdiv','dt_portfolio','normal' );
	// 	remove_meta_box( 'slugdiv','dt_portfolio','normal' );
	// 	remove_meta_box( 'trackbacksdiv','dt_portfolio','normal' );
		remove_meta_box( 'mymetabox_revslider_0', 'dt_portfolio', 'normal' );
		
		
		// VISUAL COMPOSER - TEPLETERA
		
		/* templetera */
	// 	remove_meta_box( 'mymetabox_revslider_0', 'templatera', 'normal' );
		
		
		// DOWNLOAD MANAGER
		
	// 	remove_meta_box( 'mymetabox_revslider_0', 'wpdmpro', 'normal' );
	
	}
}

add_action( 'do_meta_boxes', 'fg_remove_meta_boxes' );



/**
 * 	DASHBOARD WIDGETS
 */

function fg_remove_dashboard_meta() {
	
// 	if ( ! current_user_can( 'manage_options' ) ) {}
				
		// wordoress news
		remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
		
		// quick draft
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
		
		// at a glance
		remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
		
		// activity
		remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
		
		// remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
		// remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
		// remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
		// remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
		// remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	
}

add_action( 'admin_init', 'fg_remove_dashboard_meta' ); 



/**
 * 	ADMIN TOOLBAR (modify)
 */

function fg_remove_from_admin_toolbar() {
	
    global $wp_admin_bar;
   
	// $wp_admin_bar->remove_menu( 'wp-logo' );
    // $wp_admin_bar->remove_menu( 'comments' ); 
    // $wp_admin_bar->remove_menu( 'my-sites' ); 
    // $wp_admin_bar->remove_menu( 'site-name' ); 
    // $wp_admin_bar->remove_menu( 'new-content' );
    // $wp_admin_bar->remove_menu( 'new-post' );
    // $wp_admin_bar->remove_menu( 'new-social_icon' );
    // $wp_admin_bar->remove_menu( 'essb' );
    // $wp_admin_bar->remove_menu( 'archive' );
    // $wp_admin_bar->remove_menu( 'wpseo-menu' );
    // $wp_admin_bar->remove_menu( 'itsec_admin_bar_menu' );
    // $wp_admin_bar->remove_menu( 'ubermenu' );
    // $wp_admin_bar->remove_menu( 'maintenance_options' ); 
    
}

// add_action( 'wp_before_admin_bar_render', 'fg_remove_from_admin_toolbar' );
