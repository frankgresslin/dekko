<?php

/**
 *  INCLUDE CHILD-THEME
 *    
 *  @package include
 *  @since   1.0
 */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


// CONSTANTS

if ( !defined( 'INCLUDE_ASSETS_DIR' ) ) {
	define( 'INCLUDE_ASSETS_DIR', get_stylesheet_directory() . '/assets' );
}


if ( !defined( 'INCLUDE_ASSETS_URI' ) ) {
	define( 'INCLUDE_ASSETS_URI', get_stylesheet_directory_uri() . '/assets' );
}

 
// THEME SETUP

require_once( get_theme_file_path( '/assets/inc/child-theme-options/child-theme-setup.php' ) );


// CHILD THEME OPTIONS

// the7 customizations 
require_once( get_theme_file_path( '/assets/inc/child-theme-options/the7-template-hooks.php' ) );
require_once( get_theme_file_path( '/assets/inc/child-theme-options/the7-theme-setup.php' ) ); // stop adding post types to query

// child theme options
require_once( get_theme_file_path( '/assets/inc/child-theme-options/the7-filters.php' ) ); 

// customizer
require_once( get_theme_file_path( '/assets/inc/child-theme-options/theme-customizer.php' ) ); 


// utilities
require_once( INCLUDE_ASSETS_DIR . '/inc/utilities/utilities.php' );
require_once( INCLUDE_ASSETS_DIR . '/inc/utilities/reset.php' );
require_once( INCLUDE_ASSETS_DIR . '/inc/utilities/body-class.php' );


// require_once( INCLUDE_ASSETS_DIR . '/inc/utilities/queries.php' );
// require_once( INCLUDE_ASSETS_DIR . '/inc/utilities/header-footer-hooks.php' );
// require_once( INCLUDE_ASSETS_DIR . '/inc/utilities/widgets.php' );

// require_once( INCLUDE_ASSETS_DIR . '/inc/utilities/plugins/utilities-relevanssi.php' );

// extensions html
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/blog/html-posts-thumbnail.php' );
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/blog/html-post-meta.php' );
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/blog/html-post-excerpt.php' );
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/blog/html-pagination.php' );
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/blog/html-single.php' );

// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/search/html-search.php' );

// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/author/html-author.php' );
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/author/meta-boxes-user-profile.php' );


// THEME EXTENSIONS

// rss
require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/include-rss/include-rss.php' );

// custom site search
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/include-custom-site-search/include-custom-site-search.php' );

// related posts
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/include-related-posts/include-related-posts.php' );

// taxonomy archive widget
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/include-taxonomy-archive-widget/include-tax-archive-widget.php' );

// before & after
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/include-before-after/includes/sections.php' );
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/include-before-after/includes/before-after.php' );

// top & bottom bar navigation
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/include-top-bottom-bar-menus/include-top-bottom-bar-menus.php' );

// custom post types
// require_once( INCLUDE_ASSETS_DIR . '/inc/extensions/include-projects/include-projects.php' ); // loaded as plugin


// PLUGINS

// require_once( INCLUDE_ASSETS_DIR . '/inc/functions/plugins/utilities-gravity-forms.php' ); // needs testing
// require_once( INCLUDE_ASSETS_DIR . '/inc/functions/plugins/utilities-ajax-load-more.php' ); // needs testing



if ( is_admin() ) {
	
	// admin setup
	require_once( INCLUDE_ASSETS_DIR . '/inc/admin/admin-reset.php' );
	require_once( INCLUDE_ASSETS_DIR . '/inc/admin/admin-styles.php' );
// 	require_once( INCLUDE_ASSETS_DIR . '/inc/admin/admin-customize.php' );
		
	// editor styles
	// require_once( INCLUDE_ASSETS_DIR . '/inc/admin/editor-styles.php' ); // needs testing
	
	// meta-boxes
	// require_once( INCLUDE_ASSETS_DIR . '/inc/admin/extensions/taxonomy-fields/taxonomy-fields.php' );

}
