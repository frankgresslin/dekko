<?php
/**
 * The Template for displaying all single projects.
 * @package The7
 * @since   1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header( 'single' ); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'header-main' ); ?>

	<?php if ( presscore_is_content_visible() ): ?>

		<?php do_action( 'presscore_before_loop' ); ?>
		
		<div id="content" class="content" role="main">

			<?php if ( post_password_required() ): ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php
						
						do_action( 'presscore_before_post_content' );
	
						the_content();
	
						do_action( 'presscore_after_post_content' );
						
					?>

				</article>

			<?php else: ?>
			
				<?php 
				
				// the7 tempate part call
				get_template_part( 'content-single', str_replace( 'dt_', '', get_post_type() ) );
				
				// incude template part - calling the modified the7 tempate part
				// get_template_part( 'template-parts/content', 'single-dt_portfolio' ); 
				
				?>

			<?php endif; ?>
			
			
			
			<?php  
			
				// GET IN TOUCH
					
				$vc_custom_css = false;
			
				// get vc custom css from post meta
				$post_meta = get_post_meta( '236' );
				
				if ( isset( $post_meta[ '_wpb_shortcodes_custom_css' ][0] ) ) {
					
					$array_items = $post_meta[ '_wpb_shortcodes_custom_css' ];
					
					$vc_custom_css = '<style>';
					
					foreach ( $array_items as $array_item ) {
						$vc_custom_css .= $array_item;
					}
					
					$vc_custom_css .= '</style>';
				}
				
				echo $vc_custom_css;
				
				
				// get templetera template content
				$query = new WP_Query( array( 'post_type' => 'templatera', 'p' => '236' ) );
			
				if ( $query->have_posts() ) {
					
					while ( $query->have_posts() ) { 
						
						$query->the_post();
						
						WPBMap::addAllMappedShortcodes();
					
						the_content();
					}
				}
				
				wp_reset_postdata();
				
			?>
			
			
			
			<?php  
				
				// PROJECT CATEGORIES NAVIGATION
				
				$vc_custom_css = false;
			
				// get vc custom css from post meta
				$post_meta = get_post_meta( '545' );
				
				if ( isset( $post_meta[ '_wpb_shortcodes_custom_css' ][0] ) ) {
					
					$array_items = $post_meta[ '_wpb_shortcodes_custom_css' ];
					
					$vc_custom_css = '<style>';
					
					foreach ( $array_items as $array_item ) {
						$vc_custom_css .= $array_item;
					}
					
					$vc_custom_css .= '</style>';
				}
				
				echo $vc_custom_css;
				
				
				// get templetera template content
				$query = new WP_Query( array( 'post_type' => 'templatera', 'p' => '545' ) );
			
				if ( $query->have_posts() ) {
					
					while ( $query->have_posts() ) { 
						
						$query->the_post();
						
						WPBMap::addAllMappedShortcodes();
					
						the_content();
					}
				}
				
				wp_reset_postdata();
				
			?>
			
			
			<?php comments_template( '', true ); ?>

		</div><!-- #content -->
		
		<?php do_action( 'presscore_after_content' ); ?>

	<?php endif; // content is visible ?>

<?php endwhile; endif; // end of the loop. ?>

<?php get_footer(); ?>
