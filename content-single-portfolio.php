<?php
/**
 * Portfolio single page template.
 *
 * @package The7
 * @since  1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

	<article id="post-<?php the_ID() ?>" <?php post_class( 'project-post' ) ?>>

		<?php
		do_action( 'presscore_before_post_content' );

		presscore_get_template_part( 'mod_portfolio', 'portfolio-post-single-content' );

		do_action( 'presscore_after_post_content' );
		?>

	</article>
	
	
	<?php 
		$terms = wp_get_object_terms( get_the_ID(), 'dt_portfolio_category' );
		echo '<h3 class="projects section-header">' . __( 'More in ', 'dekko' ) . $terms[0]->slug . '</h3>';
	?>
			

<?php presscore_display_related_projects() ?>